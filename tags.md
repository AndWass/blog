---
layout: page
title: About
permalink: /tags/
---

{% for tag in site.tags %}
<h3><i class="fa fa-tag" aria-hidden="true" style="margin-right: 10px;font-size: 0.6em;"></i>{{ tag | first | capitalize }}</h3>
<ul style="list-style: none;">
  {% for p in tag[1] %}
    <li><a href="{{ p.url | prepend: site.baseurl }}">{{ p.title }}</a></li>
  {% endfor %}
</ul>
{% endfor %}

This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

You can find the source code for Minima at GitHub:
[jekyll][jekyll-organization] /
[minima](https://github.com/jekyll/minima)

You can find the source code for Jekyll at GitHub:
[jekyll][jekyll-organization] /
[jekyll](https://github.com/jekyll/jekyll)


[jekyll-organization]: https://github.com/jekyll
