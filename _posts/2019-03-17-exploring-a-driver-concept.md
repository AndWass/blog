---
"title": "Exploring a driver concept"
"date": "2019-03-17"
"layout": "post"
---

As I have mentioned before, it is common for embedded drivers to be
tightly coupled with the current hardware implementation. For instance
a driver for an I2C RTC circuit such as [NXP PCF85263](https://www.nxp.com/products/analog/interfaces/ic-bus/ic-real-time-clocks-rtc/tiny-real-time-clock-calendar-with-alarm-function-battery-switch-over-time-stamp-input-and-ic-bus:PCF85263A) together with an Atmel microcontroller will most likely use Atmels
framework (ASF for instance) directly to handle I2C communication. Now if
the microcontroller is switched to some other brand, you will have to change the
RTC driver to match the I2C driver supplied by the new brand of microcontrollers.

I have made these exact mistakes myself, and sadly I will probably continue to make
these mistakes in the future as well. Now I don't like to make excuses but I can't
help but feel that C, which is what I have done most embedded software with, isn't
helping.

## Better drivers

The tight coupling is a problem. When I develop solutions to other problems, and when
thinking of system architecture, I often
think along the lines of _"this system shouldn't care how that system operates, it should only care
about the inputs and outputs"_. The same should go for drivers! If I have an I2C RTC
circuit, the driver for the RTC circuit shouldn't care how the I2C communication is done;
it could be manually bitbanged, or use efficient DMA access, or anything else really. All
it should care about is that it can communicate with the circuite via an I2C bus.

Good drivers should also be composable. It is common for newer microcontrollers to have some
sort of RTC builtin already. However it is also common for this RTC to require more power
to continue its timekeeping when the rest of the system is off. External RTCs are very power
efficient, but require some sort of buscommunication to use. I want to see a future
where the drivers for these two RTCs easily can be combined to create a synchronized RTC.
Reading time would _usually_ fetch time from the internal RTC, and thus be fast, but every
now and then the time would be fetched and synchronized with the external RTC.

Another example would be to be able to combine two (or more) storage devices
to create a storage with the combined capacity of all its
child devices, and _where the underlying storage technology doesn't matter_.
This should then be usable in the FAT32 driver and so on.

## Polymorphism

A common approach in C is to have a `struct` with function pointers. The
`struct` provides a unified interface to interact with the driver. The
drivers in the Linux kernel are written using this technique, and Atmels ASF
also uses this approach. However this is pretty much a hand-coded version of
a C++ abstract class. In C you would have
```c++
struct i2c_chip_interface
{
    void *interface_data;

    void (*init)(void*); // interface_data sent as argument
    void (*transmit)(void*, uint8_t*, size_t);
};

// Create instances
i2c_chip_interface make_atmel_i2c();
i2c_chip_interface make_st_i2c();
// Use instances
void rtc_read(i2c_chip_interface* iface);
```
and in C++ the equivalent would be
```c++
struct i2c_chip_interface
{
    virtual ~i2c_chip_interface() = default;
    virtual void init() = 0;
    virtual transmit(std::span<std::uint8_t> to_write) = 0;
};
// Implement interface
struct atmel_i2c: public i2c_chip_interface
{
    // Implement methods...
};
struct st_i2c: public i2c_chip_interface
{
    // Implement methods...
};
// Use them
void rtc_read(i2c_chip_interface& iface);
```
This is the classical way of decoupling interface and implementation, and
I started using this approach when I wrote my first basic drivers. It
seemed right at the time. I don't have much experience writing generic code and this
closesly mimics the more familiar C driver style.

### Not all roses

There are a couple of issues with these approaches though. Performance is
likely to take a hit since the compiler can't easily inline functions as
reliably as if a direct call is made. This might not make much of a difference on
a 2GHz desktop but it can make a big difference on an embedded target
running at 4MHz and you need to implement a manually bitbanged SPI-bus.
While this is a bit of a premature optimization, in a hot code path these
things may actually matter.

Another problem is that of ownership. In a normal program you would
dynamically allocate the implementation and then transfer
ownership using smart pointers to the abstract class.
```c++
auto clk_pin{std::make_unique<atmel_pin>(1)};
// Create miso and mosi pin aswell
// "transfer ownership of pins to the software SPI
sw_spi spi{std::move(clk_pin), std::move(miso_pin), std::move(mosi_pin)};
```
Embedded targets may not have a heap though,
so you cannot assume that dynamic memory exists at all!
The actual ownership of an implementation must live
outside of the object that implicitly owns the resource.

Consider a manually bitbanged SPI bus. The bus itself requires 3 GPIO pins;
a clock, master out slave in (MOSI) and master in slave out (MISO).
A basic implementation may look something like
```c++
struct sw_spi
{
    // gpio_pin is an abstract class that provides
    // pin functionality, like setting the output value,
    // reading the current value of the pin etc.
    gpio_pin &clk_;
    gpio_pin &mosi_;
    gpio_pin &miso_;

    void write(span<uint8_t> data) {
        // write data using the pins
    }
};
```
Since `gpio_pin` is abstract we cannot store the pins as values,
we must store either pointers or references. But the pins are logically
owned by the SPI bus. Any usages outside the bus is most likely an error.
Any destruction of the pins while the SPI implementation lives is definately
an error. In some regards the C implementation can actually work around this.
I won't say its better, I won't say it is recommended, I will say that the
classic C++ way has limitations.

## Templates to the rescue?

Templates are a way of doing compile-time polymorphism. With templates
we are no longer restricted to storing pointers or references, so the
above ownership issues can be fixed.
```c++
template<class ClkPin, class MosiPin, class MisoPin>
class sw_spi
{
    ClkPin clk_;
    MosiPin mosi_;
    MisoPin miso_;
    // Rest of implementation
};
```
We don't store references to data implicitly owned by
the SPI implementation. All is actually owned by
the class. Since the compiler knows which actual pin
implementation is used for the various pins we are much
more likely to inline code that you would normally expect,
so performance may actually be better as well.

One of the biggest headaches I have had with templates is that
it has been very difficult to specify an interface for a type.
All the pins in the example above _should_ provide the same _functionality_
even if the types aren't exactly the same, but to me there haven't been
any straightforward ways to express that.

## Concepts

Concepts is an upcoming feature in C++20. It allows us to express
constraints on template arguments. I think of them as a functionality
specification for templates, and with this we can easily describe what
functionality is expected of a template argument.
```c++
namespace gpio
{
// Note that the syntax may or may not be 100% according
// to what will be in the standard. This is what ARM GCC offered
// at the time of writing this post.
template<class T>
concept bool GpioPin = requires (T t) {
    { t.configure_direction(pin::direction{}) };
    { t.configure_pull(pin::pull_config{}) };
    { t.set_value(bool{}) };
    { t.value() } -> bool;
    { t.toggle() };
};
}

template<gpio::GpioPin ClkPin,
            gpio::GpioPin MosiPin,
            gpio::GpioPin MisoPin>
class sw_spi
{
    ClkPin clk_;
    MosiPin mosi_;
    MisoPin miso_;
};
```
Now any class that satisfies the GpioPin concept can
be used with `sw_spi`, and if we have a conecpt for
a SPI bus, `sw_spi` can be used everywhere a SPI bus
is expected.

I haven't used this way of thinking alot yet, but I really like what
I have used it for so far. One of the benefits I think is that
you frame your mindset to think of drivers as composable and reusable
types, instead of implementation details on top of some interface (if that makes sense).

I can see some bumps along the road ahead, but nothing that can't be overcome.
Will concepts actually become templates for the masses? I am excited to see
how this feature will work in the embedded space, and it is my hope that I will
one day be able to write code like
```c++
// Make one storage region out of 2 flash devices
auto raw_storage = storage::join_storage(flash_device1, flash_device2);
// Reserve the first 1024 bytes for meta data
auto [meta_region, fs_region] = storage::split(raw_storage, 1024);
// The rest of available space is a FAT file system
auto fat32 = fs::make_fat32(fs_region);
```
or compose drivers in other creative ways.

I have decided to ditch the classical runtime polymorphism
during my embedded C++ development. I will
actually try to use concepts alot more instead. I will for
now stay in wonderland and try to discover how far down the
rabbit hole I can go.

If there are any errors, or if I have missed something I would
like to know! I am not in any way, shape or form an expert.
Not when it comes to programming or programming concepts.