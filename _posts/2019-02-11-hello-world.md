---
title: "Hello world"
layout: post
date: "2019-02-11"
---

My name is Andreas, I am a little over 30 years old and have always had an
interest in computers. It was quite logical that I would get into software
development; I started learning C++ as a hobby in my teenage years. My first
(and so far only) book is, shamefully, the dreaded _Learn C++ programming in 21 days_.

## Experience

I have worked as a consultant for a product development company since 2011. For
a long time I was the only software person at my work. Some electrical engineers
would also do some programming but when things became advanced they would turn to me.

The job has given me a wide range of experience, from using small Atmel AVR microcontrollers
to hacking the Linux kernel for usage in Embedded Linux, and developing Desktop applications.
I usually say I can, and will, do anything but web development.

When it comes to embedded development for microcontrollers I have mostly used C.
I had almost no prior experience when I started work and I inherited quite alot
of code that was pretty much exclusively C so it was only natural that I would
continue down the C path. C++ is starting to get more and more interesting though,
and I want to learn more around what C++ can bring to the table.

## Blog goal

The goal with this blog is to document what I learn that can be applicable to embedded C++.
My long term goal is to write an OS in C++, with focus on running on smaller ARM microcontrollers,
such as Cortex M3 or M4. The OS is intended as a learning exercise, both to learn more around ARM
microcontrollers but also what C++ can bring to the table that C struggles with. Eventually I would
like to start using C++ at work in the embedded projects we have there.
