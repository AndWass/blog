---
title: "Hello eppOS"
tags: [eppos, embedded]
date: "2019-02-12"
layout: post
---

To have an actual project project to learn from I have decided to start working on
a basic embedded OS. True to the open source community every project needs a
name, so I have decided to call the OS eppOS, which is a (bad) acronym for
_Embedded C**++** OS_. This will be my playground that I can use to play with code
and features. The goal is __not__ to develop a production-ready OS.

## Development tools

First order of business is to get a development environment up and running. I want
to be able to use both Windows and Linux. I also don't want to depend on hardware,
at least not initially.

This means that I have decided to use the following tools:

- [The official GNU ARM embedded toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm) from ARM.
- [QEMU](https://www.qemu.org/)
- [CMake](https://cmake.org/)
 - On Windows I use [Ninja](https://ninja-build.org/) as CMake generator

For developing and debugging I use [VSCode](https://code.visualstudio.com/) with the _C/C++_, _CMake_ and
_CMake Tools_ extensions.

### Installation

Everything is very straight-forward to install. On Linux there is a good chance that a package is
available in the standard package manager, so that it is enough to run something like `apt-get install gcc-arm-none-eabi`.
On Windows it is easiest to download the individual setups, which should be available for pretty much
all tools. For QEMU you will want to make sure that `qemu-system-arm` is installed.

On Windows you will probably want to add the different installation locations to your `PATH` environment
variable as well.

### CMake and VSCode

One of the main reasons I chose to use CMake and VSCode was the excellent _CMake Tools_ extensions. I have made some
minimal changes to the default settings, like setting `Ninja` as the default generator on Windows. Some
project-specific configurations have also been added but these will be explained further down.

## Choosing an MCU target

I want to keep the MCU target rather small, but also reasonably modern. In my line of work I frequently come in
contact with Cortex M MCUs, like Cortex M3 and M4. QEMU only has a few machines that targets Cortex M3s so I chose
to target the [Stellaris lm3s811evb](http://www.ti.com/lit/ds/symlink/lm3s811.pdf) machine.

## <a name="first_program"></a>First program

To test the toolchain and also test the development environment the first goal was to get a simple `Hello World`
to build and actually work in the emulator. The goal is to run this via QEMU, and somewhere get the "Hello World"
printout.
```c++
#include <cstdio>

int main() {
    std::printf("Hello World\n");
    // Remember we are the OS, so do not return from main
    while(true);
}
```
If this small program works we can actually start interacting, at least one-way, with the MCU.

## Project layout

For this first small program I assume the following structure:
```text
project
|   Source and CMake files
|
|---build
|   |   CMake build directory
|
|---.vscode
    |   VSCode configuration files
```

### CMake and toolchains

The actual first order of business is to tell CMake which compiler to use. I opted for using
a [toolchain](https://cmake.org/cmake/help/v3.12/manual/cmake-toolchains.7.html) file to have
the basic setup in a single file. After some trial and error (and googling) I ended up with the following
rather clobbered toolchain file:
```cmake
# On Windows we want to append .exe to the compiler name.
if(WIN32)
    set(CC_POSTFIX ".exe")
else()
    set(CC_POSTFIX "")
endif()

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_C_COMPILER "${ARM_COMPILER_PATH}/arm-none-eabi-gcc${CC_POSTFIX}")
set(CMAKE_CXX_COMPILER "${ARM_COMPILER_PATH}/arm-none-eabi-g++${CC_POSTFIX}")
set(CMAKE_CXX_FLAGS "-g -mthumb -mcpu=cortex-m3 -L ${CMAKE_CURRENT_LIST_DIR} -T lm3s811evb.ld")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --specs=nosys.specs --specs=nano.specs -Os -ffreestanding")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-rtti -fno-exceptions -fno-non-call-exceptions")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-common -ffunction-sections -fdata-sections")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wl,--gc-sections")
set(CMAKE_C_COMPILER_WORKS ON)
set(CMAKE_CXX_COMPILER_WORKS ON)
SET(ASM_OPTIONS "-x assembler-with-cpp")
SET(CMAKE_ASM_FLAGS "${CMAKE_CXX_FLAGS} ${ASM_OPTIONS}")
```

Now when I run CMake I only have to provide the ARM_COMPILER_PATH variable with a full path to `arm-none-eabi-{gcc, g++}`. In
VSCode this can easily be done by adding the following to the `.vscode/settings.json` file:
```json
"cmake.configureSettings": {
    "ARM_COMPILER_PATH": "C:/Program Files (x86)/GNU Tools ARM Embedded/8 2018-q4-major/bin"
}
```

To make use of the toolchain file in an integrated way a CMake kit can be defined. Create the file
`.vscode/cmake-kits.json` and add the following
```json
[{
    "name": "Local toolchain",
    "toolchainFile": "../toolchain.cmake"
}]
```

The _CMake Tools_ extensions can now automatically find and use the toolchain file when building
the project.

### Startup and linker files

For the MCU to even start our program we must make sure the executable contains some information
in the correct place. All Cortex M3s (and possibly even all Cortex Ms?) start in the same manner:
read the stack pointer from flash address `0x00` then read the value from flash address `0x04`
and jump to the corresponding address. In very basic pseudocode it does the following:
```
stack_pointer = *0x00 # *0x00 denotes uint32 value stored in location 0x00
jump *0x04
```

From there on the MCU is started and it is up to us to decide what happends next.

After much googling (yes I do that quite often) I managed to find that the ARM toolchain comes with
some samples that are an excellent starting-point. In `<installation directory>/share/gcc-arm-none-eabi/samples`
we will find both startup code and linker-scripts. And for our current scenario they are a perfect
starting point.

I copied `gcc.ld` from `ldscripts` folder and placed it in the project directory, and renamed it to
`lm3s811evd.ld` in the process.

The linker file has to be modified slightly to match our target. The original file has RAM starting at
address `0x10000000` while the lm3s811 MCU RAM starts at `0x20000000`.
```ldscript
MEMORY
{
  FLASH (rx) : ORIGIN = 0x0, LENGTH = 0x10000 /* 128K */
  RAM (rwx) : ORIGIN = 0x20000000, LENGTH = 0x2000 /* 8K */
}
```

I also copied `startup_ARMCM3.S` from `startup` and placed it in the project directory, renaming it to
simply `startup.S`. The `.S` suffix means that it contains assembly code.

I will most likely get back to these files and discuss them further as I need to both learn more,
but also when they have to be modified. For now they don't have to be modified further though.

### Retargetting

Our goal is to perform a complete retarget, where we want to emulate using `printf` to send
serial data. The ARM toolchain again comes with a basic example we can use as a starting point.
In `samples/src/retarget` we can copy both `main.c` and `retarget.c` to our project folder. Make
sure to change them to C++ files by changing the extensions to `.cpp` or similar as well.

Since C++ uses name mangling we need to disable this for the functions in `retarget.cpp` as well.
This is most easily done by adding `extern "C" {` at the top of the source file, with a closing
brace at the bottom.

What this file does is it _"overrides"_ some functions that the standard library uses to implement
functions like `printf`.

Now we must implement at least one function in a platform-specific manner; the `_write` function.
We want to emulate sending data to a serial port, or a [UART](https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter),
which is basicly a serial port but with different voltage levels. To send data via the UART port
we need to write the data to the peripheral register located on address `0x4000C000`. This can be
achieved by creating a pointer that points to this specific address: `volatile std::uint32_t* uart0dr = reinterpret_cast<std::uint32_t*>(0x4000C000);`.
This can then be used as such `*uart0dr = 'A' // Writes the character 'A'`.

The entire `_write` function is very basic:
```c++
int _write (int fd, char *ptr, int len)
{
    volatile std::uint32_t* uart0dr = reinterpret_cast<std::uint32_t*>(0x4000C000);
    /* Write "len" of char from "ptr" to file id "fd"
    * Return number of char written.
    * Need implementing with UART here. */
    (void)fd;
    for(int i=0; i<len; i++)
    {
        uart0dr = ptr[i];
    }
    return len;
}
```

Change the content of `main.cpp` to what is shown in [First program](#first_program).

## Putting it all together

The only thing missing is a CMakeLists.txt before we can build and run, and hopefully
have QEMU show us the `Hello World` we are longing for. The CMakeLists.txt is very basic,
mine only contains
```
cmake_minimum_required(VERSION 3.1)
project(eppos)
enable_language(CXX ASM)

set(CXX_STANDARD 17)
add_executable(eppos main.cpp startup.S retarget.cpp)
```

With this the content of the project directory should be
```text
CMakeLists.txt
toolchain.cmake
startup.S
lm3s811evb.ld
main.cpp
retarget.cpp

.vscode/settings.json
.vscode/cmake-kits.json
```

And that is it. With this the project should build.

### Testing

The final step is to test it in QEMU. Using a terminal located
in the project root directory the following command should work:
`qemu-system-arm -M lm3s811evb -serial stdio -s -kernel build/eppos`. The QEMU window should
show up and the terminal should print `Hello World`

![Hello world terminal]({{ site.baseurl }}/images/blog/hello_world_terminal.png)

## Summary

This was my first time documenting all the steps I had to take to get a _"basic"_ (nothing is basic
in the MCU world) `Hello World` application going. I am sure I don't follow best practices, I know
there are some shortcuts taken so the above will not work on a physical MCU.
There are probably some errors as well. But as guide to how I got everything going, this should be rather complete.

Next step will be to get a debugger running, and connecting it to the QEMU emulator. Stay tuned!
