---
title: "A basic scheduler"
date: 2019-03-03
---

In _[Multitasking 1 of n]({{ site.baseurl }}{% post_url 2019-02-23-multitasking-1 %})_ I showed
how I started implementing some very basic multitasking functionality, using cooperative
multitasking. In that post there was no real logic when switching between tasks; I
just created two tasks and switched between them in the most basic manner possible. If i wanted to add a third
task I would have to change the code in multiple places and make sure things were kept in sync. There was no easy
way to support removing tasks.
I therefore chose to start implementing my first actual scheduler; a round robin scheduler.

## Round robin scheduling

[Round robin](https://en.wikipedia.org/wiki/Round-robin_scheduling) scheduling is a fair algorithm for
task scheduling; when a task yields, the scheduler activates the next task, until all tasks have
been activated at which point the cycle is started from the beginning. This is a pretty straight-forward
algorithm, it is easy to understand how it should work and there aren't really any tricky corner-cases to
consider.

![Round robin image]({{ site.baseurl }}/images/blog/round_robin.png)

An RTOS will most likely have functionality to prioritize tasks, letting tasks with higher
priority execute before tasks with lower priority, and alot of logic to handle things like
priority inheritance. These things will be implemented eventually
but right now I am in a stage where the code changes alot and even basic code structure is
constantly evolving and changing, and I will slowly let this kind of functionality evolve.

## Building blocks

Looking at the image above we can see that we need first need to encapsulate the data of a particular task.
We also need to have some way of building a circular list of this task data. Since this is aimed at embedded
I also added a requirement that no dynamic memory shall be used, which means that no standard container is viable.

Now as seen before each task is described pretty much only by its stack, so a structure holding this data for each task
is logical.
```c++
struct task_data
{
    std::uint8_t *stack_bottom = nullptr;
    std::uint8_t *stack_pointer = nullptr;
}
```
I then needed some way of task data in a container, without having to rely on dynamic memory.
This lead me to make a very basic implementation that looked something like

```c++
struct round_robin
{
    struct task_data
    {
        std::uint8_t *stack_bottom = nullptr;
        std::uint8_t *stack_pointer = nullptr;

        task_data *next = nullptr;
        task_data *prev = nullptr;
    }

    task_data *head_;
    task_data *tail_;

    task_data create_task() {
        return {};
    }

    void add_task(task_data &task) {
        // Add it to my linked list
    }
};
```

The basic design goal was to allow the user to own the memory, but to still be able
to build a linked list of user-supplied nodes. Basicly I had "invented" a poorly made
intrusive list.

Now this worked for a short while, but soon I became frustrated because I had mixed
list-keeping logic with the scheduler-logic. I started encapsulating the list-keeping code
into its own generic class but soon got fed up and thought that there must be a solution available!

### Boosting EppOS

After some googling I found that the intrusive container library in [boost](https://www.boost.org/)
and I decided to see if their intrusive list implementation
could fit my requirements, and it did! The `boost::intrusive::list` implementation can be configured
to not use exceptions, but as always; reading documentation is key. It requires some modifications of the
stored datatype to make it usable which meant that the `round_robin_data` structure had to be adapted
```c++
namespace bint = boost::intrusive;
using intrusive_link_mode = bint::link_mode<bint::link_mode_type::normal_link>;
struct task: public bint::list_base_hook<intrusive_link_mode>
{
    // Task data goes here
};
```
This is all that is needed to enable a datatype to be usable with the intrusive list. Boost
also supports adding a member variable and use that as a hook instead, but i felt that this
was cleaner and easier to use with the actual list implementation. The member hook requires
A pointer to member as a template parameter.

One word about `link_mode_type`. 3 different link policies are supported: **normal**, **safe** and **auto unlink**.
In short, a normal link won't set the hooks to a default state when elements are erased. Containers also won't check the
hooks to make sure they are default initialized. Safe will set the hooks to a default state when elements are erased, and
containers will make sure they are set to default when inserting new elements. Auto unlink is the same as _safe_ but also
tells containers that elements can be silently erased without going through container-provided functions.

From the outset auto unlink sounds good, but it comes with trade-offs. For instance they only work with **non**-constant time
`size()` containers. As always reading documentation and evaluating different aspects is key. I chose `normal_link` above since
that is the most performant option, but I may very well opt for using the safe option later on.

With this small adaptation I can use `boost::intrusive::list` and have a type-safe, well-tested,
performant and an all-in-all well-designed intrusive list that plays nicely with standard algorithms.
I haven't done any code-size comparisons, I started implementing a very basic intrusive list on my own
but I soon got fed up with corner cases. This says something about the value of being able to use
an already existing algorithm, and even if I could implement a list-type of my own that was smaller in
code size, I am much more confident of the correctness of the boost implementation than I would be of my
own. EppOS will not suffer from NIH syndrome!

After this my `round_robin` structure, and some helpers looks something like
```c++
struct task_data
{
    typedef void(*main_fn)();
    typedef void(*end_fn)();

    main_fn main = nullptr;
    end_fn  end = nullptr;

    std::uint8_t* stack_pointer = nullptr;
    std::uint8_t* stack_bottom = nullptr;

    bool is_started = false;
};

namespace core::task_scheduler
{
    // "reset" task data. This can be done in C++ but I haven't
    // bothered to do that just yet.
    void reset(task_data &tcb) {
        asm("push {r4}"); // Push a utility registers
        asm("mov r4, lr\n"); // Store old return address
        asm("mov lr, %0\n" :: "r"(tcb.main)); // Temporarily make start_func lr

        asm("push {r3}");
        asm("mov r3, %0" :: "r"(tcb.stack_bottom));
        asm("stmdb r3!, {r4-r11, lr}\n"); // Push using "r4" as stack
        asm("mov %0, r3\n" : "=r"(tcb.stack_pointer));
        asm("pop {r3}");
        
        asm("mov lr, r4\n");
        asm("pop {r4}");
        asm("BX LR");
    }
}

namespace bint = boost::intrusive;
struct round_robin
{
    using intrusive_link_mode = bint::link_mode<bint::link_mode_type::normal_link>;
    struct task: public bint::list_base_hook<intrusive_link_mode>
    {
        task_data tcb; 
    };

    bint::list<task> task_list;
    typename bint::list<task>::iterator current_task;

    round_robin(): current_task(task_list.end()) {}

    // This function is called to create new tasks. A new task is not
    // automatically added to the list of tasks.
    task create_task(task_data::main_fn start_func,
        task_data::end_fn end_func,
        std::uint8_t *stack_bottom) {
        task retval;
        retval.tcb.main = start_func;
        retval.tcb.end = end_func;
        retval.tcb.stack_bottom = stack_bottom;
        core::task_scheduler::reset(retval.tcb);
        return retval;
    }

    void add_task(task &task) {
        // Add it to my linked list
        task_list.push_back(task);
        if(current_task == task_list.end()) {
            current_task = task_list.begin();
        }
    }

    // Requires that we have tasks, but in a kernel
    // there will always be at least 1 task present; the kernel
    // idle task.
    task& next() {            
        current_task++;
        // wraparound logic.
        if(current_task == task_list.end()) {
            current_task = task_list.begin();
        }
        return *current_task;
    }

    task& current() {
        return *current_task;
    }
};
```
From here on everything just fell in to place. The old `yield` function only required minor
modifications.
```c++
// This is the new yield
void __attribute__((noinline, naked)) yield() {
    // Store current register contexts to the current stack
    asm("push {R4-R11, LR}");
    // Store the current context stack pointer
    auto &current = rr.current(); // <- Get the current task
    asm("mov %0, sp" :"=r"(current.tcb.stack_pointer));
    // Calculate which stack to move to
    auto &next = rr.next(); // <- Get the next task from round_robin scheduler
    // Load the new stack pointer
    asm("mov sp, %0" :: "r"(next.tcb.stack_pointer));
    // Pop the registers that were stored
    asm("pop {R4-R11, LR}");
    asm("BX LR");
}
```

Creating and adding tasks is simply a matter of
```c++
std::uint8_t stack1[512];
std::uint8_t stack2[512];
std::uint8_t stack3[512];

auto task1_handle = rr.create_task(task1, nullptr, stack1+512);
auto task2_handle = rr.create_task(task2, nullptr, stack2+512);
auto task3_handle = rr.create_task(task3, nullptr, stack3+512);

rr.add_task(task1_handle);
rr.add_task(task2_handle);
rr.add_task(task3_handle);
```

And voilá we have started to implement a basic round-robin scheduler, with support for an arbitrary
number of tasks. Adding new tasks is very easy, removing tasks can easily be supported, having
multiple lists and move the tasks between them is a breeze. All thanks to C++, templates and boost.

## Some thoughts

Now I have really started to appreciate the value of using C++ over C. Doing something similar in C would not be nearly as
easy, and seriously doubt it would be as typesafe as in C++. This implementation became very straight-forward once I started using
`boost::intrusive::list`. I already have some ideas on how to implement for instance join, or sleep_for. But that is
for another day.
