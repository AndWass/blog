---
title: "Multitasking 1 of n"
date: 2019-02-23
---

One of the main features an OS provides is the ability to perform multiple tasks simultaneously,
or at least simulate it so it seems that way.

My, quite small, code base before writing this post did not provide any fascilities at all for multitasking
and I myself had never written any multitasking code for embedded either. But I wanted to at least start
getting some multitasking going. This is also a good exercise to learn more about a MCU since I have to learn
more about the gritty things like calling conventions, context switching, stacks, etc.

## Choosing a style

To try and keep things relatively simple, and easier to reason about, I chose to start with
cooperative multitasking. This is a style of multitasking that relies on tasks to be good citizens and
yield control to the kernel every now and then. A task will never be interrupted by the kernel itself.
```c++
void task_1() {
    while(true) {
        // Do some work
        yield(); // Kernel may switch task here
        // Do some more work
        yield(); // Kernel may switch task here
    }
}
```

This style is easier to reason about since a task has full control over when other tasks
may run, making things like data-races pretty much impossible (if it is possible I sure would
like an example).

## Small steps

A first step to multitasking was to get the following code to run and work:
```c++
std::uint8_t stack[2][512]; // Buffers to use as stacks

void yield() {
    // Do stuff
}

void task1() {
    int var = 1;
    std::printf("In task1 main, var=%d\n", var++);
    yield();
    std::printf("In task1 after yield, var=%d", var);
    while(true) {
        yield();
    }
}

void task2() {
    int var = 234;
    std::printf("In task2 main, var=%d\n", var++);
    yield();
    std::printf("In task2 after yield, var=%d", var);
    while(true) {
        yield();
    }
}

int main() {
    // Do something that kicks off the sequence
}
```

I would expect this to print the following output
```text
In task1 main, var=1
In task2 main, var=234
In task1 after yield, var=2
In task2 after yield, var=235
```

I know the optimizer can remove the variables, but I still felt that this was a good first
start.

## The yield function

The yield function is where most of the magic happens. What we basicly want to do is
to execute some set of instructions so that when we return from the yield function we don't
return back to where we previously came from; we want to return back into the other
main function (`task1 -> yield -> task2 -> yield -> task1` etc.) and continue where
that function left off (or start the function in the case of the first yield into `task2`).

To do this we need to understand how returns work, and how return addresses are stored when
making function calls.

When calling a function the processor uses some kind of branching instruction, and reading
the ARM instruction set we see that there are a bunch of them.
```text
B   ; Branch immediate
BL  ; Branch with link
BX  ; Branch indirect (register)
BLX ; Branch indirect with link (register)
```

Reading about these we learn that `BL` and `BLX` write the address of the next instruction,
after the branch, to the link register (`LR` or `R14`). So apparently we should be able to
modify the `LR` content to be able to control where the `yield` function returns back to.
This gives us the first clue to the puzzle.

## Contexts

Before we can fill in the rest of the puzzle we must know more about the registers.
Specifically we need to know what registers must be saved before jumping to a new function,
so that we can resume the old function correctly again. Storing the current state of a processor
and restoring some old state is called `context switching`. In its most basic form its simply
the act of storing some values to some storage, and restoring new values from a different storage before resuming execution. Hopefully we are now executing as if the old state was never left.

So to dig into how to accomplish this I had to read up on how context switches works on an ARM
Cortex M3. I quickly learned that the EABI calling convention says that the registers
`R4-R11` together with `LR` are _callee-saved_. That is, they must be saved by a function before they can be used
inside the function, and they must be restored to their old values before returning from the function.
The other registers are _caller-save_ and must be stored before branching.

## The stack

Each context can be associated with a stack. A stack is a blob of memory that the processor can `push` and `pop` values
to/from. The MCU has yet another special register, the stack-pointer (`SP`), that keeps track of the stack. This register
is incremented or decremented automatically with each `push/pop` execution. Now that means we also need to keep track of
the stack-pointer each time we perform a content switch, and each task should have its own stack since that is part of its
execution state.

We can however utilize the stack to store the register values for us, and since pushing and poping is something very common
there are utility instructions that can help us with this.
```text
push {R4-R11, LR} ; push values from registers R4 to R11 and LR on to the stack
pop {R4-R11, LR} ; pop values from the stack into registers R4 to R11 and LR
```

### Push and pop

`push` and `pop` are actually short-hand instructions for the `stmdb sp!, <reglist>` and `ldmia sp!, <reglist>` where
`stmdb` means _store multiple registers, decrement address before access_ and `ldmia` means _load multiple registers, increment address after access_.
`sp!` means that the address should be read from `sp` and the newly calculated address is also written back to `sp` again. `<reglist>`
can be for instance `{R4-R11, LR}` or something similar.
In C-style pseudo-code this can be described as
```c++
std::uint32_t *sp;
// stmdb
sp--;
*sp = register_value;
// ldmia
register_value = *sp;
sp++;
```

In reality the pointer points to a single byte though, not a 32 bit value as in the code above.

This makes it possible to use some other register than `sp` and still get the same functionality, which can come in handy
when initializing a stack for a new task.

## The first context switch
```c++
#include <cstdio>
#include <cstdint>

std::uint8_t stack[2][512];
// Let the stack pointers point to the bottom of the stacks
// Remember that a push decrements the pointer before "dereferencing"
std::uint8_t *sp[2] = {stack[0] + 512, stack[1] + 512};
int current_task = 0;

// Attributes noinline and naked tells the compiler that
// it shouldn't inline the function, and naked tells it to
// not generate any prolouge or epilouge, such as pushing/popping
// registers it intends to use. Since the first and last thing we
// do is a complete context save/restore, everything in between can freely
// use the registers as it pleases since they will be scratched anyway.
void __attribute__((noinline, naked)) yield() {
    // Store current register contexts to the current stack
    asm("push {R4-R11, LR}");
    // Store the current context stack pointer
    asm("mov %0, sp" :"=r"(sp[current_task]));
    // Calculate which stack to move to
    current_task = (current_task + 1) % 2;
    // Load the new stack pointer
    asm("mov sp, %0" :: "r"(sp[current_task]));
    // Pop the registers that were stored
    asm("pop {R4-R11, LR}");
    asm("BX LR");
}

void task1() {
    int var = 1;
    std::printf("In task1 main, var=%d\n", var++);
    yield();
    std::printf("In task1 after yield, var=%d\n", var);
    while(true) {
        yield();
    }
}

void task2() {
    int var = 234;
    std::printf("In task2 main, var=%d\n", var++);
    yield();
    std::printf("In task2 after yield, var=%d\n", var);
    while(true) {
        yield();
    }
}

[[noreturn]] int main() {
    // Setup the stack for task2 so that a yield will start it
    asm("mov sp, %0" :: "r"(sp[1]));
    asm("mov lr, %0" :: "r"(task2));
    // R* registers contains garbage, but we don't really
    // care. We simply want the pop executed in yield to
    // pop the correct value into LR, so we push
    // an entire context to the stack but only LR is of importance.
    asm("push {R4-R11, lr}");
    asm("mov %0, sp" :"=r"(sp[1]));
    // Set SP to be stack of task1 and branch to its main
    // no need to perform any special setup since we just do a
    // a branch immediate. The yield will take care of the correct
    // book-keeping for us.
    asm("mov sp, %0" :: "r"(sp[0]));
    asm("mov r0, %0" :: "r"(task1));
    // Off we go
    asm("BX r0");
    // Will never get here but we mark main with noreturn and
    // loop here anyway.
    while(true);
}
```

And the output is indeed
```
In task1 main, var=1
In task2 main, var=234
In task1 after yield, var=2
In task2 after yield, var=235
```

## Finally

This was a first context switch using cooperative task switching. From here it is
a long road before we have even a remotely usable OS, nevermind a real-time OS,
but small progress is still progress. Next I want to work on tying in a scheduler with
all this, at which point using C++ instead of C might make more of a difference. Also
stuff like task joining will come next.
