---
layout: page
---

## [eppOS](eppos)

Learning exercise to learn more around embedded OS development.
Developed using C++.

Code available on [gitlab](https://gitlab.com/AndWass/eppos).
